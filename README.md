# Surefire & Failsafe Bug

## Setup
This project is set up to contain four source sets: one for production code and three for test code:

```
src
├── blackboxtest
│   └── java
├── integrationtest
│   └── java
├── main
│   └── java
└── test
    └── java
```

The tests of each test set should be executed by its own, hence we want to have three executions of surefire and/or failsafe, each executing one test set.

## Configuration
According to [the documentation][surefireDoc] ("*By default, Surefire will only scan for test classes to execute in the configured `testSourceDirectory`.*", this should be possible by defining executions in the [`pom.xml`][pomXml] like so:

```xml
<project>
  ...
  <build>
    ...
    <plugins>
      ...
      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${surefire-plugin.version}</version>
      </plugin>
      <plugin>
        <artifactId>maven-failsafe-plugin</artifactId>
        <version>${surefire-plugin.version}</version>
        <executions>
          <execution>
            <id>integration-test</id>
            <goals>
              <goal>integration-test</goal>
              <goal>verify</goal>
            </goals>
            <phase>${failsafe.it-phase}</phase>
            <configuration>
              <includes>
                <include>**/*.java</include>
              </includes>
              <testSourceDirectory>${project.basedir}/src/integrationtest/java</testSourceDirectory>
            </configuration>
          </execution>
          <execution>
            <id>blackbox-test</id>
            <goals>
              <goal>integration-test</goal>
              <goal>verify</goal>
            </goals>
            <phase>${failsafe.bt-phase}</phase>
            <configuration>
              <includes>
                <include>**/*.java</include>
              </includes>
              <testSourceDirectory>${project.basedir}/src/blackboxtest/java</testSourceDirectory>
            </configuration>
          </execution>
        </executions>
      </plugin>
      ...
    </plugins>
    ...
  </build>
  ...
</project>
```

(important bits here are the definitions of the `testSourceDirectory` to direct surefire/failsafe to only execute those tests. Notice that we do not need to configure the `testSourceDirectory` for `surefire` since it by default points to `${project.build.sourceDirectory}`, i.e `./src/test/java`.)

## Test
If we, however, execute the tests

```bash
./mvnw clean verify
```

we see that all three tests are executed in all three executions:

```
[INFO] --- maven-surefire-plugin:3.0.0-M5:test (default-test) @ surefire-failsafe-test ---
[INFO]
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running BlackboxTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.2 s - in BlackboxTest
[INFO] Running IntegrationTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.005 s - in IntegrationTest
[INFO] Running UnitTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.006 s - in UnitTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 3, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO]
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ surefire-failsafe-test ---
[INFO] Building jar: /mnt/x/git/surefire-failsafe-test/target/surefire-failsafe-test-1.0-SNAPSHOT.jar
[INFO]
[INFO] --- maven-failsafe-plugin:3.0.0-M5:integration-test (integration-test) @ surefire-failsafe-test ---
[INFO]
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running BlackboxTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.118 s - in BlackboxTest
[INFO] Running IntegrationTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.013 s - in IntegrationTest
[INFO] Running UnitTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 s - in UnitTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 3, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO]
[INFO] --- maven-failsafe-plugin:3.0.0-M5:verify (integration-test) @ surefire-failsafe-test ---
[INFO]
[INFO] --- maven-failsafe-plugin:3.0.0-M5:integration-test (blackbox-test) @ surefire-failsafe-test ---
[INFO]
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running BlackboxTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.091 s - in BlackboxTest
[INFO] Running IntegrationTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.005 s - in IntegrationTest
[INFO] Running UnitTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.009 s - in UnitTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 3, Failures: 0, Errors: 0, Skipped: 0
```

# Expected behaviour:
- In execution `maven-surefire-plugin:3.0.0-M5:test (default-test)`, only `UnitTest` is executed,
- in execution `maven-failsafe-plugin:3.0.0-M5:integration-test (integration-test)`, only `IntegrationTest` is executed, and
- in execution `maven-failsafe-plugin:3.0.0-M5:integration-test (blackbox-test)`, only `BlackboxTest` is executed.

## Observed behaviour:
All three tests are executed on every execution.

## Workaround
If we uncomment all comments in the [`pom.xml`][pomXml], we get the expected behaviour.

## Reported
An [issue][issue] was created.

[surefireDoc]: https://maven.apache.org/surefire/maven-surefire-plugin/examples/inclusion-exclusion.html
[pomXml]: ./pom.xml
[issue]: https://issues.apache.org/jira/browse/SUREFIRE-2013